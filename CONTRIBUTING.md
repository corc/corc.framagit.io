# Guide de contribution

## Nom des versions

Ce dépôt ne suit pas le modèle de gestion sémantique des versions

Étant donné que nous avons affaire à du texte, et non à un programme,
l'évolution des numéros serait beaucoup trop rapide!

* Je propose, dans un premier temps, de simplement utiliser:
   * x.y.z ou
      * **x** = l'année
      * **y** = le mois
      * **z** = le jour
         * **-[a-z]** = la version dans la journée


## Messages de commit

Selon un article original de [DotnetDojo](https://www.dotnetdojo.com/git-commit/)

Afin que l'historique de ce projet soit utilisable dans le temps j'estime
important que les messages soient formatés suivant certaines règles.

Les buts :

- avoir un contenu homogène dans les commits, qui ne dépende pas de
  l'humeur, du stress ou de la personne ;
- une capacité d'être analysé par un humain et par un script basique ;
- permettre une entrée (ou une reprise après une longue pause) plus
  facile dans le projet.


### Rédaction d'un message de commit

Chaque message est enregistré selon la forme suivante:

	<type>(<portée>): <sujet>
	<LIGNE BLANCHE>
	<corp>
	<LIGNE BLANCHE>
	<pied>

Où:

- **type** est une description codée de l'action du commit qui DOIT
  être choisie parmi les valeurs suivantes qui signalent :
  - **feat** : l'ajout d'une nouvelle fonctionnalité,
  - **fix** : la correction d'un bogue ou d'une erreur dans le
    code ou le contenu textuel,
  - **docs** : des modifications ou ajouts uniquement dans la
    documentation,
  - **style** : les modifications ne portent que sur le style ou le
    formatage du code et pas sur un changement de sens du code,
  - **refactor** : un changement qui n'est ni une nouvelle
    fonctionnalité ni une correction de bogue,
  - **perf** : une modification qui améliore les performances ou qui
    simplifie le code sans changer le sens ou le résulta,
  - **chore** : des modifications dans les outils annexes ou sur les
    processus de compilations,
  - **test** : des modifications ou des ajouts sur les tests ;
- **portée** indique les composants ou le périmètre des modifications
  (le nom d'un fichier, d'un composant, une idée comme "pagination"...) ;
- **Sujet** une description succincte sans majuscule et ponctuation
  finale avec des verbes à l'impératif ou au participe passé ;
- **corps** décrit les raisons qui ont amené à faire ces changements
  par rapport au commit précédent ;
- **pied** est une option qui permet d'indiquer un numéro de bogue,
  de décrire le bogue ou de signaler les changements qui casse la rétro
  compatibilité.

Il serait bon d'établir (ici) une liste des **portées** utilisables.


## ChangeLog

Ce projet suit strictement la convention [changelog](https://keepachangelog.com/fr/0.3.0/)


## Conventions de nommage

### Arborescence de fichier

#### docs

Dans ce projet généré par MkDocs, c'est le dossier racine du contenu.

Tout ce qui doit être dans le futur site doit être placé ici.


####  documentation

Placez dans *documentation* et ses sous-répertoires toute la documentation
afférente au projet, sans oublier les notes et courriers électroniques
importants. Vous pouvez avoir des sous-répertoires de doc contenant
différents types de documents ou pour différentes phases du projet.

Si vous avez besoin de documentation externe, envisager de la copier
ici. Cela rendra service pour maintenir le projet si l'endroit où les
données en question étaient accessibles disparaît.


#### util

Répertoire contenant les utilitaires, outils et scripts spécifiques
au projet.


#### vendor

Si le projet utilise des bibliothèques fournies par une partie tierce
ou des fichiers d'en-têtes que vous désirez archiver avec votre code,
faites-le ici.


###  Branche dans GIT (WorkFlow)

* Ce projet utilise Git-flow au pied de la lettre:
   * L'article de base qui donnera naissance au projet :
      * http://nvie.com/posts/a-successful-git-branching-model/
   * Aide mémoire français (et en d'autres traductions) :
      * https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html
   * Le module git qui permet de l'appliquer :
      * https://github.com/petervanderdoes/gitflow-avh

Enfin, pour la structure générale… Mais comme il s'agit de texte,
je propose, dans un premier temps, de tester l'ajout de texte par le
système de _hotfix_.


#### Branche **master**
Elle représente le dernier état installable en production du
projet. Seul le créateur du dépôt peut travailler dans cette branche.


#### Branche **develop**
La branche où est récolté le travail de tout le monde, des branches
de développement privées. Seul la "Team" peut travailler dans cette
branche.


#### Les branches **feature**
Chaque branche doit être nommée de la manière suivante:

* PSEUDO-DESCRIPTION

où:

* **PSEUDO** est le pseudo de l'administrateur (le créateur) de la branche
* **DESCRIPTION** Une description en CamelCase (RaisonCreationBranche)
    de cette branche


#### Les autres branches
Les branches, tags et versions qui sont poussés sur
le serveur sont **toutes** documentés dans le dossier:
`/doc/(featrure|bugfix|release|hotfix|support|tags)/`

