Page d'acueil site perso
========================

*date de création* : 2022/07/10


Destinataires du projet
-----------------------

Projet à usage personnel.


Ce que le projet fait
---------------------

Production d'un site web statique à l'aide de mkdocs.


Prérequis
---------

- git
  - git flow (usage strict)
- mkdocs
- mkdocs-materials


Documentation
-------------

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [LICENSE](LICENSE) termes de la licence


### Documentation généraliste
#### Git


#### GitFlow


#### LaTex


